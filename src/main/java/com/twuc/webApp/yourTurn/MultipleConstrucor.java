package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstrucor {
    private Dependent dependent;

    @Autowired
    public MultipleConstrucor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstrucor(String s){

    }

    public Dependent getDependent() {
        return dependent;
    }
}
