package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class TreeWithDependencyLeaves {
    private Leaves leaves;

    public TreeWithDependencyLeaves(Leaves leaves) {
        this.leaves = leaves;
    }

    public Leaves getLeaves() {
        return leaves;
    }
}
