package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AnnotationConfigApplicationContextTest {

    @Test
    void should_create_object_without_dependency_successfully_using_annotationConfigApplicationContext(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        TreeWithoutDependency treeWithoutDependency = context.getBean(TreeWithoutDependency.class);

        assertNotNull(treeWithoutDependency);
    }

    @Test
    void should_create_objects_with_dependency_successfully_using_annotationConfigApplicationContext(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        TreeWithDependencyLeaves treeWithDependency = context.getBean(TreeWithDependencyLeaves.class);

        assertNotNull(treeWithDependency);
        assertNotNull(treeWithDependency.getLeaves());
    }

    @Test
    void should_fail_to_create_object_out_of_scanning_scope_using_annotationConfigApplicationContext(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("OutOfScanningScope");

        try{
            TreeWithDependencyLeaves treeWithDependency = context.getBean(TreeWithDependencyLeaves.class);
        }catch (NoSuchBeanDefinitionException e){
            return;
        }
        fail();
    }

    @Test
    void should_create_interfaceImpl_successfully_using_interface_class (){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

//        InterfaceImpl interfaceImpl = (InterfaceImpl) context.getBean(Interface.class);
//        assertNotNull(interfaceImpl);

        Interface interfaceInstance = context.getBean(Interface.class);
        assertNotNull(interfaceInstance);
    }

    @Test
    void should_get_name_when_get_simplestInterface_bean(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        SimpleObject simpleObject = (SimpleObject) context.getBean(SimpleInterface.class);
        assertEquals("O_o", simpleObject.getSimpleDependent().getName());
    }

    //2.2
    @Test
    void should_use_autowired_constructor_when_given_more_than_one_constructors(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        MultipleConstrucor multipleConstrucor = context.getBean(MultipleConstrucor.class);
        assertNotNull(multipleConstrucor.getDependent());
    }

    @Test
    void should_call_both_constructor_and_autowired_method(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        WithAutowiredMethod withAutowiredMethod = context.getBean(WithAutowiredMethod.class);
        assertNotNull(withAutowiredMethod.getDependent());
        //???
        assertNull(withAutowiredMethod.getAnotherDependent());
    }

    void should_get_all_implements_when_getBean_Interface_class(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Map<String, InterfaceWithMultipleImpls> interfaceWithMultipleImpls = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        assertEquals(3, interfaceWithMultipleImpls.size());
    }
}
